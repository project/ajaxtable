Ajax Table module:
-------------------------
Author - Chris Shattuck (www.impliedbydesign.com)
License - GPL


Overview:
-------------------------
The Ajax Table module is an API-based tool that will allow other modules to create ajax-refreshable tables by supplying a few parameters. There are some handy utilities built in that aid in managing data more quickly than many Drupal modules offer by default. With an Ajax Table, it is easy to add ajax-based input columns to change data on the fly. There is also a utility that works similarly to an auto-complete field, but displays results in a 
fully customizable Ajax table, which supports pagination and in-place 
editing.


Benefits of Ajax Table:
-------------------------
- Allows users to browse quickly through data without numerous page loads.
- API-based, so it requires little code to do some heavy lifting.
- Specify which columns are searchable, and it will generate a search box
automatically to search those columns.
- Easily add more specific search criteria like a date range, or title
matching. Searches are still loaded via AJAX.
- AJAX-based pagination
- Easy API to create 'input columns', where you can create columns that
will alter data via AJAX. One example would be a checkbox to mark a 
node as published.
- Integrated 'edit' and 'delete' columns if specified. If the table is
browsing nodes or users, it's just a matter of adding a parameter to add
'edit' and 'delete' columns. For quick testing, the delete column will
delete via AJAX as well.
- Integration with JS Tools pop-up calendar, which allows easier input
of date-based search criteria or ajax-based data editing.
- Completely themable. While you can change the visual appearance of 
an AJAX table via CSS, you can also specify an actual layout as well. 
There are two core themes that come with AJAX table, but another module can
easily specify additional themes. Themes can also be overridden on a
per-table basis.
- Includes utilities to add an auto-complete style input to forms. The
input is rendered as a radio box, rather than a 'click-to-add-to-input'
style. This can help reduce confusion and limit the choices of a user
to what is actually available in a database table. Also, since it works
just like an AJAX table, you can use all the other utilities available.
- Decent documentation exists. AJAX table comes with a module called
Ajaxtable Docs that includes a number of examples and code snippets.
- Integration with a hover-over tooltip module called Cluetip for 
easy-to-read and flexible integrated help.


Installation:
-------------------------
- Download the Ajaxtable module and copy it into your 'modules'
directory. 
- Go to Administer >> Modules and enable the module, as well as
the Ajaxtable Docs module, which includes comprehensive 
documentation.
- If you will be using the autocomplete or tooltip features of 
ajaxtable, then you need two items. Note that I have only done moderate
testing with this, but it is possible that using the new jQuery may break
something:
	1) Install the jquery-update module. This will allow backwards compatability
     with the newest jQuery udpdate.
	2) Copy the jquery.js file from the ajaxtable module folder and paste
	   it into the misc/ folder in your Drupal installation, replacing the
		 old jquery.js. The new jquery.js is 1.2.1.
- To learn how to use AJAX Table, read through the docs at
mysite.com/?q=ajaxtable/docs or, if clean URLS are enabled, at
mysite.com/ajaxtable/docs.


For further documentation:
-------------------------
Please install the Ajaxtable Docs module that comes with the AJAX Table.
This page, which can be found at ajaxtable/docs, includes several examples
and lists of available parameters.


Last updated:
------------
