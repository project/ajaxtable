<?php
$theme = array(

	'button_text' => array(
			'search' => 'Go',
			'jump' => 'Jump',
			'next' => 'Next',
			'prev' => 'Prev',
		),
	
	'nav' =>  '
			<table class="ajaxtable-input-page-nav-table">
				<tr>
					<td class="ajaxtable-input-nav-td"><div class="ajaxtable-input-nav-overall">{prev}{pages}{next}</div></td>
					<td class="ajaxtable-input-jump-td">{jump_form}</td>
					<td class="ajaxtable-input-info-td">{total_pages} pages / {rows} rows </td>
				</tr>
			</table>
			',
	
	'search' =>  '
			<table class="ajaxtable-input-search-table">
				<tr>
					<td>{search_input}&nbsp;</td>
					<td>{search_button}</td>
					<td>{search_help}</td>
					<td class="ajaxtable-input-loading-td">{loading_div}</td>
				</tr>
			</table>
			',

	'th' => '<table><tr><td>{help}</td><td class="ajaxtable-input-th-label">{label}</td><td>{asc}</td><td>{desc}</td></tr></table>',

	'overall' => '{search}{table}{nav}',
	
);
?>