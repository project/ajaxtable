<?php
$theme = array(

	'button_text' => array(
			'search' => 'Go',
			'jump' => 'Jump',
			'next' => 'Next',
			'prev' => 'Prev',
		),
	
	'nav' =>  '
			<table class="ajaxtable-default-page-nav-table">
				<tr>
					<td class="ajaxtable-default-nav-td"><div class="ajaxtable-default-nav-overall">{prev}{pages}{next}</div></td>
					<td class="ajaxtable-default-jump-td">{jump_form}</td>
					<td class="ajaxtable-default-info-td">{total_pages} pages / {rows} rows total</td>
					<td class="ajaxtable-default-loading-td">{loading_div}</td>
				</tr>
			</table>
			',
	
	'search' =>  '
			<table class="ajaxtable-default-search-table">
				<tr>
					<td class="ajaxtable-default-search-table-left"></td>
					<td>{search_help}</td><td>Search for {search_input}&nbsp;</td>
					<td>{search_button}</td>
					<td class="ajaxtable-default-search-table-right"></td>
				</tr>
			</table>
			',

	'th' => '<table><tr><td>{help}</td><td class="ajaxtable-default-th-label">{label}</td><td>{asc}</td><td>{desc}</td></tr></table>',

	'overall' => '{search}{nav}{table}',
	
);
?>